import React, { Component } from 'react';

/* components */
import Paragraph from './building_blocks/Paragraph'
import Scroll from './building_blocks/Scroll'
import gifts from '../logic/gifts'

class Result extends Component {

    render() {

        const { totals } = this.props;
        const sortedTotals = Object.keys(totals).sort( 
            (a,b) => totals[b] - totals[a]
        );
        console.log(">>> sortedTotals: ", sortedTotals)
        return (
            <Scroll>
                <Paragraph>
                    Your results are:
                </Paragraph>
                {
                    sortedTotals.slice(0, 5).map( 
                        (trait, index) => 
                            <Paragraph>
                                {index + 1}. 
                                {gifts[trait].gift}
                            </Paragraph>
                    )
                }
            </Scroll>
        );
    }
}

export default Result;
