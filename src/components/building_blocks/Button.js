import styled from 'styled-components';

const Button = styled.button`
    width: 140px;
    height: 140px;
    border: 2px solid #fff;
    background-color: ${ props => props.clicked? "#fff" : "rgba(0,0,0,0)"};
    outline: none;
    color: ${ props => props.clicked? "rgb(12, 56, 200)" : "#fff"};
    font-size: 30px;
    margin-left: 15px;
    margin-right: 15px;
    transition: 0.25s all;
    &:hover {
        background-color: #fff;
        color: rgb(12, 26, 200)
    }
    &:active {
        background-color: #fff;
        color: rgb(12, 26, 200)
    }
`
export default Button;