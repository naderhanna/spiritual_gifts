import React from 'react';
import styled from 'styled-components';

const left_arrow = require('../assets/left-arrow.png');
const right_arrow = require('../assets/right-arrow.png');

const ArrowImg= styled.img`
    position: fixed;
    top: 50%;
    ${
        props => props.left ? "left: 40px" : ""
    }
    ${
        props => props.right ? "right: 40px" : ""
    }
    height: 210px;
    margin-top: -105px;
    opacity: ${ props => props.enabled? "1" : "0.25"}
`

const Arrow = (props) => {
    const { enabled, onClick } = props;
    return(
        <React.Fragment>
            { props.left &&
                <ArrowImg onClick={onClick} enabled={enabled} left src={left_arrow} />
            }
            { props.right &&
                <ArrowImg onClick={onClick} enabled={enabled} right src={right_arrow} />
            }
        </React.Fragment>
    )
}

export default Arrow;