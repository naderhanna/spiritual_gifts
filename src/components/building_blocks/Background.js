import styled from 'styled-components';

const Background = styled.div`
    width: 100wh;
    height: 100vh;
    background-color: rgb(12, 56, 200);
    ${'' /* background-image: url(${gif}); */}
    background-size: cover;
    overflow: hidden;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
`

export default Background;