import styled from 'styled-components';

const Row = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
`

export default Row;