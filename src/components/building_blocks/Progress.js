import styled from 'styled-components';

const Progress = styled.div`
    color: #fff;
    font-size: 40px;
    font-weight: lighter;
    position: fixed;
    top: 40px;
    left: 50%;
    transform: translateX(-50%);
`

export default Progress;