import styled from 'styled-components';

const Paragraph = styled.p`
    color: #fff;
    font-size: 100px;
    width: 100%;
    text-shadow: 0px 0px 400px rgba(0,0,0,1);
    font-weight: lighter;
    letter-spacing: 1px;
    margin: 10px 0;
    padding: 0;
`
export default Paragraph;