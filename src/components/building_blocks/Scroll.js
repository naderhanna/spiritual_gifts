import styled from 'styled-components';

const Scroll = styled.div`
    width: 100%;
    text-align: center;
    max-width: 1200px;
    margin-left: auto; 
    margin-right: auto;
    height: 100%;
    overflow: scroll;
    display: flex;
    flex-direction: column;
    justify-content: center;
`

export default Scroll;