import styled from 'styled-components';

const Question = styled.h1`
    align-self: flex-end;
    color: #fff;
    font-size: 55px;
    width: 100%;
    max-width: 800px;
    text-align: center;
    text-shadow: 0px 0px 400px rgba(0,0,0,1);
    font-weight: lighter;
    letter-spacing: 1px;
`
export default Question;