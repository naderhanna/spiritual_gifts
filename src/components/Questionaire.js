import React, { Component } from 'react';

/* components */
import Question from './building_blocks/Question'
import Button from './building_blocks/Button'
import Row from './building_blocks/Row'

class Questionaire extends Component {


    render() {
        const { question, onAnswer, answer } = this.props;
        const questionText = question.question;
        const target = question.target;
        return (
            <React.Fragment>
            <Question>
            {questionText}
            </Question>
            <Row>
                <Button  clicked={answer === 1} onClick={() => onAnswer(target, 1)}>
                    NO
                </Button>
                <Button  clicked={answer === 2} onClick={() => onAnswer(target, 2)}>
                    LITTLE
                </Button>
                <Button  clicked={answer === 3} onClick={() => onAnswer(target, 3)}>
                    SOME
                </Button>
                <Button  clicked={answer === 4} onClick={() => onAnswer(target, 4)}>
                    MUCH
                </Button>       
            </Row>
            </React.Fragment>
        );
    }
}

export default Questionaire;
