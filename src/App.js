import React, { Component } from 'react';

/* components */
import Background from './components/building_blocks/Background';
import Questionaire from './components/Questionaire'
import Results from './components/Results'
import Progress from './components/building_blocks/Progress'
import Arrow from './components/building_blocks/Arrow';
import quiz from './logic/build_quiz';

class App extends Component {

  constructor(props){
    super(props)
    this.state = {
      currentQuestion: 0,
      questionReached: 0,
      totals: {},
      answers: []
    }
  }

  nextQuestion = () => {
    const { currentQuestion, questionReached } = this.state;
    if (
      currentQuestion < quiz.length &&
      currentQuestion < questionReached
    ) {
      this.setState({
        currentQuestion: this.state.currentQuestion + 1
      })
    }
  }

  previousQuestion = () => {
    const { currentQuestion } = this.state;
    if (currentQuestion > 0) {
      this.setState({
        currentQuestion: this.state.currentQuestion - 1
      })
    }
  }

  onAnswer = (target, value) => {
    const { currentQuestion, totals, answers, questionReached } = this.state;
    const r = questionReached
    const i = currentQuestion; 
    if (i < quiz.length) {
      const score = totals[target]? totals[target] : 0
      this.setState({
        currentQuestion: i + 1,
        questionReached: Math.max(i + 1, r),
        answers: [
          ...answers.slice(0,i), 
          value, 
          ...answers.slice(i + 1,)
        ],
        totals: {
          ...totals,
          [target]: score + value
        }
      })
    }
  }



  render() {

    const {currentQuestion, answers, questionReached, totals} = this.state;
    const q = currentQuestion;
    const r = questionReached;

    return (
      <Background>
        <Arrow enabled={q > 0} left onClick={this.previousQuestion} />
        <Arrow enabled={q < r} right onClick={this.nextQuestion} />
        <Progress>
            { currentQuestion + 1 } / { quiz.length }
        </Progress>
        { r === quiz.length ? 
          <Results
            totals={totals} /> :
          <Questionaire
            question={quiz[currentQuestion]}
            onAnswer={this.onAnswer}
            answer={q <= r? answers[q] : null}
        />
        }
      </Background>
    );
  }
}

export default App;
