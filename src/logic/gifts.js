const gifts = {
    'a': {
        'gift': 'serving',
        'explanation': '',
        scripture: ''
    },
    'b': {
        'gift': 'helping',
        'explanation': '',
        scripture: ''
    },
    'c': {
        'gift': 'mercy',
        'explanation': '',
        scripture: ''
    },
    'd': {
        'gift': 'hospitality',
        'explanation': '',
        scripture: ''
    },
    'e': {
        'gift': 'tongues',
        'explanation': '',
        scripture: ''
    },
    'f': {
        'gift': 'healing',
        'explanation': '',
        scripture: ''
    },
    'g': {
        'gift': 'exhortation',
        'explanation': '',
        scripture: ''
    },
    'h': {
        'gift': 'giving',
        'explanation': '',
        scripture: ''
    },
    'i': {
        'gift': 'wisdom',
        'explanation': '',
        scripture: ''
    },
    'j': {
        'gift': 'knowledge',
        'explanation': '',
        scripture: ''
    },
    'k': {
        'gift': 'faith',
        'explanation': '',
        scripture: ''
    },
    'l': {
        'gift': 'apostle & missionary',
        'explanation': '',
        scripture: ''
    },
    'm': {
        'gift': 'evangelism',
        'explanation': '',
        scripture: ''
    },
    'n': {
        'gift': 'prophecy',
        'explanation': '',
        scripture: ''
    },
    'o': {
        'gift': 'teaching',
        'explanation': '',
        scripture: ''
    },
    'p': {
        'gift': 'pastoring',
        'explanation': '',
        scripture: ''
    },
    'q': {
        'gift': 'leadership',
        'explanation': '',
        scripture: ''
    },
    'r': {
        'gift': 'administration',
        'explanation': '',
        scripture: ''
    },
    's': {
        'gift': 'Miracles',
        'explanation': '',
        scripture: ''
    },
    't': {
        'gift': 'Interpretation of Tongues',
        'explanation': '',
        scripture: ''
    },
}

export default gifts;