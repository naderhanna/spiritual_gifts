import questions from './questions';

const  targets = ("abcdefghijklmnopqrst").split("");

const quiz = questions.map( (question, index)  => {
    return ({
        target: index >= 20? targets[index % 20] : targets[index],
        question: question
    })
})

export default quiz